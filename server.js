const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
const port = process.env.PORT || 3000;

var app = express();

hbs.registerPartials(__dirname + '/views/partials');
hbs.registerHelper('year', () => {
    return new Date().getFullYear();
});
hbs.registerHelper('Capital',(text) => {
    return text.toUpperCase();
});

app.set('view engine', 'hbs');

app.use((req,res,next)=>{
    var now = new Date().toString();
    var log = `${now}: ${req.method} ${req.url}`;
    console.log(log);
    fs.appendFileSync('server.log',log + '\n');
    next();
});

// app.use((req,res,next)=>{
//     res.render('maintainence.hbs');
    
// });

app.use(express.static(__dirname + '/public'));

app.get('/',(req,res) => {
    res.render('home.hbs', {
        pageTitle: 'HomePage',
        Welcome: 'Welcome Home',
       
    })
});

app.get('/about',(req,res)=> {
    res.render('about.hbs', {
        pageTitle : 'AboutPage',
       
    });
});

app.get('/portfolio',(req,res)=> {
    res.render('portfolio.hbs',{
        pageTitle: 'Portfolio',
        role: 'Portfolio',
    })
})

app.get('/bad',(req,res)=>{
    res.send({
        errorMessage: 'bad response'
    });
});

app.listen(port, ()=>{
    console.log(`Server Started on ${port}`);
});